#Source and examples: https://github.com/micha/resty

#Install
curl -L http://github.com/micha/resty/raw/master/resty > resty

#Source it every time you nice to use it (in the command line or a shell script)
. /path/to/resty

#set env token access to Buildkite
TOKEN=5f0449820577e4c195d6a44392bda9f881d85396
 
#Set base url and autorization header using env TOKEN
resty https://api.buildkite.com/v2 -H 'Authorization: Bearer $TOKEN'
 
#Get builds :)
GET /builds
