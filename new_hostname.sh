#!/bin/bash

#export $V_HOSTNAME=$1
export CURRENT=$(hostname)
hostname $V_HOSTNAME
echo "$V_HOSTNAME" > /etc/hostname
sed -i -e "s/$CURRENT/$V_HOSTNAME/g" /etc/hosts
systemctl restart systemd-logind.service
hostnamectl set-hostname $V_HOSTNAME
