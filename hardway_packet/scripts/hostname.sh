V_HOSTNAME=$1
V_SHORT=$2

#Set hostname
hostname $V_HOSTNAME
echo "$V_HOSTNAME" > /etc/hostname
sed -i -e "s/$V_SHORT.local.lan/$V_HOSTNAME/g" /etc/hosts
#sed -i -e "s//$V_SHORT/g" /etc/hosts
systemctl restart systemd-logind.service
hostnamectl set-hostname $V_HOSTNAME
