#From controller0 / Packet.net config
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/bin/etcd --name controller0 \
  --cert-file=/etc/etcd/kubernetes.pem \
  --key-file=/etc/etcd/kubernetes-key.pem \
  --peer-cert-file=/etc/etcd/kubernetes.pem \
  --peer-key-file=/etc/etcd/kubernetes-key.pem \
  --trusted-ca-file=/etc/etcd/ca.pem \
  --peer-trusted-ca-file=/etc/etcd/ca.pem \
  --initial-advertise-peer-urls https://10.99.121.5:2380 \
  --listen-peer-urls https://10.99.121.5:2380 \
  --listen-client-urls https://10.99.121.5:2379,http://127.0.0.1:2379 \
  --advertise-client-urls https://10.99.121.5:2379 \
  --initial-cluster-token etcd-cluster-0 \
  --initial-cluster controller0=https://10.99.121.5:2380,controller1=https://10.99.121.3:2380,controller2=https://10.99.121.1:2380 \
  --initial-cluster-state new \
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
