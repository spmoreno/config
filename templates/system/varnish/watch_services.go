package main

import (
    "fmt"
    "time"
    //"io/ioutil"
    //"os"

    "k8s.io/client-go/kubernetes"
    "k8s.io/client-go/pkg/api/v1"
    "k8s.io/client-go/tools/cache"
    "k8s.io/client-go/rest"
    "k8s.io/client-go/pkg/fields"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func svcCreated(obj interface{}) {
        svc := obj.(*v1.Service)
        fmt.Println("Action: CREATE / Namespace: "+svc.ObjectMeta.Namespace+ " / Name: "+svc.ObjectMeta.Name)

}

func svcDeleted(obj interface{}) {
        svc := obj.(*v1.Service)
        fmt.Println("Action: DELETED / Namespace: "+svc.ObjectMeta.Namespace+ " / Name: "+svc.ObjectMeta.Name)

}


func main() {

    // creates the in-cluster config
        config, err := rest.InClusterConfig()
    if err != nil {
        panic(err.Error())
    }
    clientset, err := kubernetes.NewForConfig(config)
    if err != nil {
        panic(err.Error())
    }

    watchlist := cache.NewListWatchFromClient(clientset.Core().RESTClient(), "services", v1.NamespaceAll,
        fields.Everything())
    _, controller := cache.NewInformer(
        watchlist,
        &v1.Service{},
        time.Second * 0,
        cache.ResourceEventHandlerFuncs{
            AddFunc: svcCreated,
            DeleteFunc: svcDeleted,
            UpdateFunc:func(oldObj, newObj interface{}) {
                fmt.Printf("service changed \n")
            },
        },
    )
    stop := make(chan struct{})
    go controller.Run(stop)
    for{
        time.Sleep(time.Second)
    }
}
