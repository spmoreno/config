#!/bin/bash

curl -O https://kubernetes-helm.storage.googleapis.com/helm-v2.1.3-linux-amd64.tar.gz \
&& tar -xzvf helm-v2.1.3-linux-amd64.tar.gz && mv ./linux-amd64/helm /usr/local/bin/helm && rm -r linux-amd64 

#init helm and install tiller in the current cluster
helm init
