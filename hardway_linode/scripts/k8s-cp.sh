#!/bin/bash
# Install a master on a 3 node etcd cluster plus k8s control plane

#export ETCD_NAME=$1
export INTERNAL_IP=$1

#echo $ETCD_NAME
#echo $INTERNAL_IP

#exit 0


#Colour config
RED='\033[0;31m'
NC='\033[0m' # No Color

#message function
msg(){
  printf "${RED}############################${NC}\n"
  printf "$1\n"
  printf "${RED}############################${NC}\n"
  sleep 1
}

msg "Copy Certificates for k8s control plane to /var/lib/kubernetes"
sudo mkdir -p /var/lib/kubernetes
sudo cp ca.pem kubernetes-key.pem kubernetes.pem /var/lib/kubernetes/

msg "Downloading k8s binaries"
wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kube-apiserver \
&& wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kube-controller-manager \
&& wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kube-scheduler \
&& wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kubectl

msg "Install k8s binaries"
chmod +x kube-apiserver kube-controller-manager kube-scheduler kubectl
sudo mv kube-apiserver kube-controller-manager kube-scheduler kubectl /usr/bin/

msg "Security - Download and moving default token"
wget https://raw.githubusercontent.com/kelseyhightower/kubernetes-the-hard-way/master/token.csv
sed -i s/chAng3m3/Comen1320./g token.csv #Change default token
sudo mv token.csv /var/lib/kubernetes/

msg "Security - Downloading and moving authorization policy file"
wget https://raw.githubusercontent.com/kelseyhightower/kubernetes-the-hard-way/master/authorization-policy.jsonl
sudo mv authorization-policy.jsonl /var/lib/kubernetes/

msg "Create the systemd unit file kube-apiserver.service"
cat > kube-apiserver.service <<"EOF"
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
ExecStart=/usr/bin/kube-apiserver \
  --admission-control=NamespaceLifecycle,LimitRanger,SecurityContextDeny,ServiceAccount,ResourceQuota \
  --advertise-address=INTERNAL_IP \
  --allow-privileged=true \
  --apiserver-count=3 \
  --authorization-mode=ABAC \
  --authorization-policy-file=/var/lib/kubernetes/authorization-policy.jsonl \
  --bind-address=INTERNAL_IP \
  --enable-swagger-ui=true \
  --etcd-cafile=/var/lib/kubernetes/ca.pem \
  --insecure-bind-address=INTERNAL_IP \
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \
  --etcd-servers=https://10.99.121.1:2379,https://10.99.121.3:2379,https://10.99.121.5:2379 \
  --service-account-key-file=/var/lib/kubernetes/kubernetes-key.pem \
  --service-cluster-ip-range=192.145.0.0/18 \
  --service-node-port-range=30000-32767 \
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \
  --token-auth-file=/var/lib/kubernetes/token.csv \
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sed -i s/INTERNAL_IP/$INTERNAL_IP/g kube-apiserver.service

msg "Moving kube-apiserver.service to /etc/systemd/system/ and start service"
sudo mv kube-apiserver.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable kube-apiserver
sudo systemctl start kube-apiserver
sudo systemctl status kube-apiserver --no-pager

msg "Now Controller Manager"
cat > kube-controller-manager.service <<"EOF"
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
ExecStart=/usr/bin/kube-controller-manager \
  --cluster-name=kubernetes \
  --leader-elect=true \
  --master=http://INTERNAL_IP:8080 \
  --root-ca-file=/var/lib/kubernetes/ca.pem \
  --service-account-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \
  --service-cluster-ip-range=192.145.0.0/18 \
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sed -i s/INTERNAL_IP/$INTERNAL_IP/g kube-controller-manager.service

msg "Moving kube-controller-manager.service to /etc/systemd/system/ and starting component"
sudo mv kube-controller-manager.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable kube-controller-manager
sudo systemctl start kube-controller-manager
sudo systemctl status kube-controller-manager --no-pager

msg "Last one - Kubernetes Scheduler"
cat > kube-scheduler.service <<"EOF"
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
ExecStart=/usr/bin/kube-scheduler \
  --leader-elect=true \
  --master=http://INTERNAL_IP:8080 \
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sed -i s/INTERNAL_IP/$INTERNAL_IP/g kube-scheduler.service

msg "Moving kube-scheduler.service to /etc/systemd/system/ and starting"
sudo mv kube-scheduler.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable kube-scheduler
sudo systemctl start kube-scheduler
sudo systemctl status kube-scheduler --no-pager

msg "Lastly, setup kubectl and check health verification kubectl get componentstatuses"
kubectl config set-cluster cow-cluster --server=http://$INTERNAL_IP:8080 --api-version=v1
kubectl config set-context cow-context --cluster=cow-cluster --user=admin --namespace=default
kubectl config use-context cow-context
kubectl get componentstatuses
