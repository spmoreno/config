#!/bin/bash
# Install a master on a 3 node etcd cluster plus k8s control plane

#export WORKER_NAME=$2
export INTERNAL_IP=$1

#echo $ETCD_NAME
#echo $INTERNAL_IP

#exit 0


#Colour config
RED='\033[0;31m'
NC='\033[0m' # No Color

#message function
msg(){
  printf "${RED}############################${NC}\n"
  printf "$1\n"
  printf "${RED}############################${NC}\n"
  sleep 1
}

#msg "Setting up hostname"
#Set hostname
#hostname $WORKER_NAME
#echo "$WORKER_NAME" > /etc/hostname
#sed -i -e "s/$WORKER_NAME.local.lan//g" /etc/hosts
#systemctl restart systemd-logind.service
#hostnamectl set-hostname $WORKER_NAME

msg "Copy Certificates for k8s control plane to /var/lib/kubernetes"
sudo mkdir -p /var/lib/kubernetes
sudo cp ca.pem kubernetes-key.pem kubernetes.pem /var/lib/kubernetes/

msg "Downloading Docker binaries"
wget https://get.docker.com/builds/Linux/x86_64/docker-1.12.3.tgz
tar -xvf docker-1.12.3.tgz
sudo cp docker/docker* /usr/bin/

msg "Create the Docker systemd unit file"
sudo sh -c 'echo "[Unit]
Description=Docker Application Container Engine
Documentation=http://docs.docker.io

[Service]
ExecStart=/usr/bin/docker daemon \
  --iptables=false \
  --ip-masq=false \
  --host=unix:///var/run/docker.sock \
  --log-level=error \
  --storage-driver=overlay
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/docker.service'

sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl start docker

sudo docker version

msg "Installing CNI"
sudo mkdir -p /opt/cni
wget https://storage.googleapis.com/kubernetes-release/network-plugins/cni-07a8a28637e97b22eb8dfe710eeae1344f69d16e.tar.gz
sudo tar -xvf cni-07a8a28637e97b22eb8dfe710eeae1344f69d16e.tar.gz -C /opt/cni

msg "K8s binaries"
wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kubectl \
&& wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kube-proxy \
&& wget https://storage.googleapis.com/kubernetes-release/release/v1.4.6/bin/linux/amd64/kubelet

chmod +x kubectl kube-proxy kubelet
sudo mv kubectl kube-proxy kubelet /usr/bin/
sudo mkdir -p /var/lib/kubelet/

msg "Configuring Kubelet"
sudo sh -c 'echo "apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority: /var/lib/kubernetes/ca.pem
    server: https://192.168.207.108:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubelet
  name: kubelet
current-context: kubelet
users:
- name: kubelet
  user:
    token: Comen1320." > /var/lib/kubelet/kubeconfig'

cat /var/lib/kubelet/kubeconfig

msg "Create the kubelet systemd unit file"
sudo sh -c 'echo "[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=docker.service
Requires=docker.service

[Service]
ExecStart=/usr/bin/kubelet \
  --allow-privileged=true \
  --api-servers=https://192.168.207.108:6443,https://192.168.214.248:6443,https://192.168.221.117:6443 \
  --cloud-provider= \
  --cluster-dns=10.145.0.10 \
  --cluster-domain=cluster.local \
  --address=INTERNAL_IP \
  --node-ip=INTERNAL_IP \
  --container-runtime=docker \
  --docker=unix:///var/run/docker.sock \
  --network-plugin=cni \
  --cni-conf-dir=/etc/cni/net.d \
  --kubeconfig=/var/lib/kubelet/kubeconfig \
  --serialize-image-pulls=false \
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \
  --v=2

Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/kubelet.service'

sed -i s/INTERNAL_IP/$INTERNAL_IP/g /etc/systemd/system/kubelet.service

cat /etc/systemd/system/kubelet.service

sudo systemctl daemon-reload
sudo systemctl enable kubelet
sudo systemctl start kubelet
sudo systemctl status kubelet --no-pager

msg "Configuring kube-proxy"
sudo sh -c 'echo "[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
ExecStart=/usr/bin/kube-proxy \
  --master=https://192.168.207.108:6443 \
  --kubeconfig=/var/lib/kubelet/kubeconfig \
  --proxy-mode=iptables \
  --v=2

Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/kube-proxy.service'

cat /etc/systemd/system/kube-proxy.service

sudo systemctl daemon-reload
sudo systemctl enable kube-proxy
sudo systemctl start kube-proxy
sudo systemctl status kube-proxy --no-pager

#Necesario para weave http://askubuntu.com/questions/645638/directory-proc-sys-net-bridge-missing
modprobe br_netfilter
