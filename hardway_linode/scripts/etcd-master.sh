#!/bin/bash
# Install a master on a 3 node etcd cluster plus k8s control plane

export ETCD_NAME=$1
export INTERNAL_IP=$2

#echo $ETCD_NAME
#echo $INTERNAL_IP

#exit 0


#Colour config
RED='\033[0;31m'
NC='\033[0m' # No Color

#message function
msg(){
  printf "${RED}############################${NC}\n"
  printf "$1\n"
  printf "${RED}############################${NC}\n"
  sleep 2
}

msg "Copy Certificates for etcd"
sudo mkdir -p /etc/etcd/
sudo cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/

msg "Downloading etcd"
wget https://github.com/coreos/etcd/releases/download/v3.0.15/etcd-v3.0.15-linux-amd64.tar.gz
tar -xvf etcd-v3.0.15-linux-amd64.tar.gz
sudo mv etcd-v3.0.15-linux-amd64/etcd* /usr/bin/

# All etcd data is stored under the etcd data directory. In a production cluster the data directory should be backed by a persistent disk.
# Create the etcd directory:
msg "Creating etcd config"
sudo mkdir -p /var/lib/etcd
cat > etcd.service <<"EOF"
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/bin/etcd --name ETCD_NAME \
  --cert-file=/etc/etcd/kubernetes.pem \
  --key-file=/etc/etcd/kubernetes-key.pem \
  --peer-cert-file=/etc/etcd/kubernetes.pem \
  --peer-key-file=/etc/etcd/kubernetes-key.pem \
  --trusted-ca-file=/etc/etcd/ca.pem \
  --peer-trusted-ca-file=/etc/etcd/ca.pem \
  --initial-advertise-peer-urls https://INTERNAL_IP:2380 \
  --listen-peer-urls https://INTERNAL_IP:2380 \
  --listen-client-urls https://INTERNAL_IP:2379,http://127.0.0.1:2379 \
  --advertise-client-urls https://INTERNAL_IP:2379 \
  --initial-cluster-token etcd-cluster-0 \
  --initial-cluster controller0=https://10.99.121.1:2380,controller1=https://10.99.121.3:2380,controller2=https://10.99.121.5:2380 \
  --initial-cluster-state new \
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sed -i s/INTERNAL_IP/${INTERNAL_IP}/g etcd.service
sed -i s/ETCD_NAME/${ETCD_NAME}/g etcd.service

msg "Moving etcd config and initiating etcd service"
sudo mv etcd.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable etcd
sudo systemctl start etcd
sudo systemctl status etcd --no-pager

#Once all 3 etcd nodes have been bootstrapped verify the etcd cluster is healthy:
#etcdctl --ca-file=/etc/etcd/ca.pem cluster-health
