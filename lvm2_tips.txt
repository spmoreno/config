#Installing LVM && Thin tools && To format logical volumes created with lvcreate
apt install -y lvm2 thin-provisioning-tools xfsprogs

#Activating services
systemctl enable lvm2-lvmetad.service && systemctl enable lvm2-lvmetad.socket && systemctl start lvm2-lvmetad.service && systemctl start lvm2-lvmetad.socket

#volume group && Logical thinpool of 13G in vg 'vg001'
vgcreate -f vg001 /dev/sdc && lvcreate -L 13G -T vg001/mythinpool01

#logical volumen 'thinvol01' of 3G in thinpool 'mythinpool01'
lvcreate -V 3G -T vg001/mythinpool01 -n thinvol01

#exporting to later mount the brick && format && mounting
mkdir -p /mnt/thinvol01 && mkfs.xfs -i size=512 -n size=8192 /dev/vg001/thinvol01 && mount /dev/vg001/thinvol01 /mnt/thinvol01

#creating the brick dir && adding entry to /etc/fstab
mkdir /mnt/thinvol01/brick01 && echo "/dev/vg001/thinvol01 /mnt/thinvol01 xfs defaults 0 0"  >> /etc/fstab

#installing glusterfs
apt-get install -y software-properties-common

#Then add the community GlusterFS PPA:
add-apt-repository -y ppa:gluster/glusterfs-3.8 && apt-get update

#Finally, install the server packages
apt-get install -y glusterfs-server

#now create trusted pool storage
gluster peer probe XXX.XXX.XXX.XXX #This command for each server IP or hostname

#create a replica 3 testvol -> vol001
gluster volume create vol001 replica 3 192.168.140.112:/mnt/thinvol01/brick01 192.168.219.228:/mnt/thinvol01/brick01 192.168.131.150:/mnt/thinvol01/brick01

#start new vol001 volume
gluster volume start vol001



##################### Client #########################
#Add the GlusterFS PPA && install gluster client
apt install -y  software-properties-common && add-apt-repository -y ppa:gluster/glusterfs-3.8 && apt-get update && apt-get install -y glusterfs-client

#mount volume on client
mkdir -p /vol/vol001 &&  mount -t glusterfs 192.168.219.228:/vol001 /vol/vol001
