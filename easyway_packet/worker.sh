#!/bin/bash
# Install a master

export WORKER_NAME=$(hostname -s)
export INTERNAL_IP=$(ip addr | awk '/inet / {sub(/\/.*/, "", $2); print $2}' | grep '10.')

#Colour config
RED='\033[0;31m'
NC='\033[0m' # No Color

#message function
msg(){
  printf "${RED}############################${NC}\n"
  printf "$1\n"
  printf "${RED}############################${NC}\n"
  sleep 1
}

msg "Actualizando packages"
apt-get update && apt-get upgrade -y
apt-get install -y apt-transport-https

msg "Setting up hostname"
#Set hostname
hostname $WORKER_NAME
echo "$WORKER_NAME" > /etc/hostname
sed -i -e "s/$WORKER_NAME.local.lan//g" /etc/hosts
systemctl restart systemd-logind.service
hostnamectl set-hostname $WORKER_NAME

msg "Installing Docker and K8s dependencies"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update

msg "Install docker if you don't have it already"
apt-get install -y docker.io
apt-get install -y kubelet kubeadm kubectl kubernetes-cni

msg "Seguridad basica (cambio puerto SSHD y no uso de password)"
#SEGURIDAD
#Disable password auth
sed -i -e "s/#PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config
#Change ssh port
sed -i -e "s/Port 22/Port 22240/g" /etc/ssh/sshd_config
#restart sshd deamon
systemctl reload sshd

msg "Creando aliases"
echo "alias lsa='ls -la'" >> /root/.bashrc
echo "alias c='clear'" >> /root/.bashrc
echo "alias gp='kubectl get pods --all-namespaces -o wide'" >> /root/.bashrc
echo "alias gs='kubectl get svc --all-namespaces -o wide'" >> /root/.bashrc
echo "alias gi='kubectl get ingress --all-namespaces -o wide'" >> /root/.bashrc
echo "alias grc='kubectl get rc --all-namespaces -o wide'" >> /root/.bashrc
echo "alias gd='kubectl get deployments --all-namespaces -o wide'" >> /root/.bashrc
echo "alias dp='kubectl describe pod'" >> /root/.bashrc
echo "alias ds='kubectl describe svc'" >> /root/.bashrc
echo "alias drc='kubectl describe rc'" >> /root/.bashrc
echo "alias dd='kubectl describe deployment'" >> /root/.bashrc
echo "alias di='kubectl describe ingress'" >> /root/.bashrc
echo "alias k='kubectl'" >> /root/.bashrc
source /root/.bashrc

msg "Ultimo update && upgrade"
apt-get update && apt-get -y upgrade && apt -y autoremove

msg "Lunching kubeadm init binded to private ip"
kubeadm init --api-advertise-addresses=$INTERNAL_IP > kubeadm.log
#kubeadm init > kubeadm.log

msg "Lunching calico cni"
kubectl apply -f https://raw.githubusercontent.com/spmoreno/config/master/templates/system/calico_kubeadm_packet.yaml
 
msg "Finalizado"
